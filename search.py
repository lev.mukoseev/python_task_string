class BrutForce:
    @staticmethod
    def find(string, substring):
        """Поиск грубой силой (Bruteforce)"""
        result = []
        if not string or not substring:
            return result
        n = len(string)
        m = len(substring)
        for i in range(n - m + 1):
            match = 0
            while string[i + match] == substring[match]:
                match += 1
                if match == m:
                    break
            if match == m:
                result.append(i)
        return result


class Hash:
    """Поиск с использованием Hash-функции"""
    @staticmethod
    def find(string, substring, update_hash, ceil=10**9 + 7):
        result = []
        if not string or not substring:
            return result
        pattern_hash = 0
        string_hash = 0
        n = len(string)
        m = len(substring)
        for i in range(m):
            pattern_hash = update_hash(pattern_hash, substring, i, m) % ceil
            string_hash = update_hash(string_hash, string, i, m) % ceil

        for i in range(m, n + 1):
            if (pattern_hash == string_hash
                    and is_coincidence(string, substring, i - m)):
                result.append(i - m)
            if i == n:
                break
            string_hash = update_hash(string_hash, string, i, m) % ceil
        return result


def Rabin_Karp(cur_hash, string, index, sub_len):
    """Hash-функция Рабина-Карпа"""
    const = 2
    if index >= sub_len:
        return ((cur_hash - ord(string[index - sub_len]))
                / const + ord(string[index]) * const ** (sub_len - 1))
    return cur_hash + ord(string[index]) * const ** index


def line_hash(cur_hash, string, index, sub_len):
    """Линейная Hash-функция"""
    if index >= sub_len:
        return cur_hash - ord(string[index - sub_len]) + ord(string[index])
    return cur_hash + ord(string[index])


def square_hash(cur_hash, string, index, sub_len):
    """"Квадратичная Hash-функция"""
    if index >= sub_len:
        return cur_hash - ord(string[index - sub_len])**2 + ord(string[index])**2
    return cur_hash + ord(string[index])**2


def power_hash(_, string, index, sub_len):
    """Полностью пересчитываемая Hash-функция"""
    if index + 1 < sub_len:
        return 0
    s = 0
    for i in range(sub_len):
        s += ord(string[index - sub_len + 1 + i])**(i + 1)
    return s


class Automaton:
    """Поиск с использованием автомата"""
    def __init__(self, string, substring):
        self.string = string
        self.substring = substring
        self.n = len(string)
        self.m = len(substring)
        self.alph = self.get_alph()
        self.table = self.get_table()
        self.result = self.get_result()

    def get_alph(self):
        alph = {}
        for i in self.substring:
            alph[i] = 0
        return alph

    def get_table(self):
        table = {}
        for i in range(self.m + 1):
            table[i] = {}
        for i in self.alph:
            table[0][i] = 0
        for i in range(self.m):
            previous = table[i][self.substring[i]]
            table[i][self.substring[i]] = i + 1
            for j in self.alph:
                table[i + 1][j] = table[previous][j]
        return table

    def get_result(self):
        result = []
        index = 0
        for i in range(self.n):
            if self.string[i] in self.alph:
                index = self.table[index][self.string[i]]
                if index == self.m:
                    result.append(i - index + 1)
            else:
                index = 0
        return result


class BoyerMoore:
    """Поиск с помощью алгоритма Боера-Мура"""
    def __init__(self, string, pattern):
        self.string = string
        self.pattern = pattern
        self.m = len(pattern)
        self.rpr = self.get_rpr()
        self.bad_char = self.get_bad_char()
        self.shift_table = self.get_shift_table()
        self.result = self.get_result()

    def get_bad_char(self):
        table = {}
        for i in range(len(self.pattern)):
            table[self.pattern[i]] = self.m - i - 1
        return table

    def is_equal(self, start, end, cur_char):
        for i in range(start, end + 1):
            if (i < self.m
                    or self.pattern[i - self.m] == self.pattern[cur_char]):
                cur_char += 1
            else:
                return False
        return True

    def check(self, k, i):
        first = self.is_equal(
            k + self.m - 1,
            k + self.m + i - 2,
            self.m - i)
        second = k > 1 and self.pattern[k - 2] != self.pattern[self.m - i - 1]
        return first and (second or k <= 1) and k <= self.m - i

    def get_rpr(self):
        rpr = []
        for i in range(self.m + 1):
            for k in range(self.m - i + 1, -self.m, -1):
                if self.check(k, i):
                    rpr.append(k)
                    break
        return rpr

    def get_shift_table(self):
        shift = []
        if self.m == 0:
            return
        for i in range(self.m + 1):
            shift.append(self.m - self.rpr[i] - i + 1)
        return shift

    def get_result(self):
        result = []
        if self.m == 0:
            return result
        i = 0
        match = 0
        while True:
            if i + self.m > len(self.string):
                break
            for j in range(i + self.m - 1, i - 1, -1):
                if self.string[j] == self.pattern[j - i]:
                    match += 1
                    if match == self.m:
                        result.append(i)
                        i += self.shift_table[match]
                        match = 0
                        break
                else:
                    if self.string[j] not in self.bad_char:
                        shift1 = self.m
                    else:
                        shift1 = self.bad_char[self.string[j]]
                    shift2 = self.shift_table[match]
                    i += max(shift1, shift2, 1)
                    match = 0
                    break
        return result


class KnuthMorrisPratt:
    """Поиск с помощью алгоритма Кнута-Морриса-Пратта"""
    def __init__(self, string, pattern):
        self.string = string
        self.pattern = pattern
        self.shift_table = self.get_shift_table()
        self.result = self.get_result()

    def get_shift_table(self):
        shifts = [1] * (len(self.pattern) + 1)
        shift = 1
        for i in range(len(self.pattern)):
            while shift <= i and self.pattern[i] != self.pattern[i - shift]:
                shift += shifts[i - shift]
            shifts[i + 1] = shift
        return shifts

    def get_result(self):
        result = []
        if len(self.pattern) == 0:
            return result
        start = 0
        match = 0
        for i in self.string:
            while match == len(self.pattern) \
                    or match >= 0 and self.pattern[match] != i:
                start += self.shift_table[match]
                match -= self.shift_table[match]
            match += 1
            if match == len(self.pattern):
                result.append(start)
        return result


class Raita:
    """Поиск с помощью алгоритма Райта"""
    def __init__(self, string, pattern):
        self.string = string
        self.pattern = pattern
        self.bad_char = self.get_bad_char()
        self.result = self.get_result()

    def get_bad_char(self):
        table = {}
        for i in range(len(self.pattern)):
            table[self.pattern[i]] = len(self.pattern) - i - 1
        return table

    def get_result(self):
        result = []
        if len(self.pattern) == 0:
            return result
        index = 0
        pattern_len = len(self.pattern)
        first = self.pattern[0]
        middle = self.pattern[pattern_len // 2]
        last = self.pattern[-1]
        while index <= len(self.string) - pattern_len:
            last_char = self.string[index + pattern_len - 1]
            if (last_char == last
                    and self.string[index + pattern_len // 2] == middle
                    and self.string[index] == first
                    and is_coincidence(self.string, self.pattern, index)):
                result.append(index)
            if last_char not in self.bad_char:
                index += pattern_len
            else:
                index += max(self.bad_char[last_char], 1)
        return result


def is_coincidence(string, pattern, start):
    index = 0
    while string[index + start] == pattern[index]:
        index += 1
        if index == len(pattern):
            return True
    return False
