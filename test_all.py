import search as s
import unittest


def get_results(string, substring):
    return {
        "BrutForce": s.BrutForce.find(string, substring),
        "Line Hash": s.Hash.find(string, substring, s.line_hash),
        "Square Hash": s.Hash.find(string, substring, s.square_hash),
        "R-K Hash": s.Hash.find(string, substring, s.Rabin_Karp),
        "Full Update Hash": s.Hash.find(string, substring, s.power_hash),
        "Automaton": s.Automaton(string, substring).result,
        "BoyerMoore": s.BoyerMoore(string, substring).result,
        "KnuthMorrisPratt": s.KnuthMorrisPratt(string, substring).result,
        "Raita": s.Raita(string, substring).result
    }


str_eq_start, substr_eq_start, result_eq_start = "abcdefregtynhjm", "abc", [0]
str_empty, substr_empty, result_empty_strings = "", "", []
str_eq_end, substr_eq_end, result_eq_end = "abcdefregtynhjm", "hjm", [12]
str_sev_entries, substr_sev_entries, result_sev_entries = ('avcxfswdsfsw',
                                                           'fsw',
                                                           [4, 9])


class SpecialTests(unittest.TestCase):
    unittest.TestCase.maxDiff = None

    def test_bad_char_BM(self):
        bad_char = s.BoyerMoore("a", "abcdasawss").bad_char
        expected = {'a': 3, 'b': 8, 'c': 7, 'd': 6, 's': 0, 'w': 2}
        self.assertEqual(bad_char, expected)

    def test_bad_char_Raita(self):
        bad_char = s.Raita("a", "abcdasawss").bad_char
        expected = {'a': 3, 'b': 8, 'c': 7, 'd': 6, 's': 0, 'w': 2}
        self.assertEqual(bad_char, expected)

    def test_rpr_BM(self):
        rpr = s.BoyerMoore("a", "abraca").rpr
        expected = [6, 4, 0, -1, -2, -3, -4]
        self.assertEqual(rpr, expected)

    def test_table_Automaton(self):
        table = s.Automaton("", "abraca").table
        expected = {
            0: {'a': 1, 'b': 0, 'r': 0, 'c': 0},
            1: {'a': 1, 'b': 2, 'r': 0, 'c': 0},
            2: {'a': 1, 'b': 0, 'r': 3, 'c': 0},
            3: {'a': 4, 'b': 0, 'r': 0, 'c': 0},
            4: {'a': 1, 'b': 2, 'r': 0, 'c': 5},
            5: {'a': 6, 'b': 0, 'r': 0, 'c': 0},
            6: {'a': 1, 'b': 2, 'r': 0, 'c': 0}}
        self.assertEqual(table, expected)


class TestSearch(unittest.TestCase):
    unittest.TestCase.maxDiff = None

    def test_eq_start(self):
        results = get_results(str_eq_start, substr_eq_start)
        for method_name in results:
            self.assertEqual(results[method_name], result_eq_start)

    def test_eq_end(self):
        results = get_results(str_eq_end, substr_eq_end)
        for method_name in results:
            self.assertEqual(results[method_name], result_eq_end)

    def test_empty_str(self):
        results = get_results(str_empty, substr_eq_start)
        for method_name in results:
            self.assertEqual(results[method_name], result_empty_strings)

    def test_empty_substr(self):
        results = get_results(str_eq_start, substr_empty)
        for method_name in results:
            self.assertEqual(results[method_name], result_empty_strings)

    def test_sev_entries(self):
        results = get_results(str_sev_entries, substr_sev_entries)
        for method_name in results:
            self.assertEqual(results[method_name], result_sev_entries)


class TestBasicFunctions(unittest.TestCase):
    unittest.TestCase.maxDiff = None

    def test_is_coincidence_true(self):
        self.assertTrue(s.is_coincidence("abracadabra", "aca", 3))

    def test_is_coincidence_false(self):
        self.assertFalse(s.is_coincidence("abracadabra", "rab", 3))


if __name__ == '__main__':
    unittest.main()
