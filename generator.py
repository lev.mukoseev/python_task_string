import string as string_module
import random


def random_string(string_length):
    return ''.join(random.choices(string_module.ascii_letters,
                                  k=string_length))


def worst_brute_force(string_length, substring_length):
    substring = 'a' * substring_length
    string = 'a' * string_length
    return string, substring


def best_brute_force(string_length, substring_length):
    substring = 'a' * substring_length
    string = 'b' * string_length
    return string, substring


def worst_hash(string_length, substring_length):
    substring = '\U0010ffff' * substring_length
    string = '\U0010ffff' * string_length
    return string, substring


def best_hash(string_length, substring_length):
    substring = '!' * substring_length
    string = ' ' * string_length
    return string, substring


def worst_Raita(string_length, substring_length):
    substring = 'a' * substring_length
    string = 'a' * string_length
    return string, substring


def best_Raita(string_length, substring_length):
    substring = 'a' * substring_length
    string = 'b' * string_length
    return string, substring


def worst_BoyerMoore(string_length, substring_length):
    return worst_Raita(string_length, substring_length)


def best_BoyerMoore(string_length, substring_length):
    return best_Raita(string_length, substring_length)
