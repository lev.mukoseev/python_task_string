import search as s
import generator as g
import time
import xlwt


def measure_time(string, substring, func, repeat, length, subfunc=None):
    times = []
    if subfunc is None:
        for _ in range(repeat):
            start = time.perf_counter()
            func(string[:length], substring)
            end = time.perf_counter()
            times.append(end - start)
    else:
        for _ in range(repeat):
            start = time.perf_counter()
            func(string[:length], substring, subfunc)
            end = time.perf_counter()
            times.append(end - start)

    t_mid = sum(times) / repeat
    s = (sum(map(lambda t: (t - t_mid)**2, times)) / (repeat - 1))**0.5
    return t_mid, s


def get_measures(string, substring, func, repeat, lengths, subfunc=None):
    return [measure_time(string, substring, func, repeat, length, subfunc)
            for length in lengths]


def get_main_dependence(string,
                        substring,
                        dstr_len,
                        repeat,
                        func,
                        name,
                        subfunc=None):
    xlist = [i for i in range(0, len(string) + 1, dstr_len)]
    measures = get_measures(string, substring, func, repeat, xlist, subfunc)
    ylist = [x[0] for x in measures]
    inaccuracies = [x[1] for x in measures]
    ws = wb.add_sheet(name)
    ws.write(0, 0, "String Length")
    ws.write(0, 1, "Search Time")
    for i in range(len(xlist)):
        ws.write(i + 1, 0, xlist[i])
        ws.write(i + 1, 1, ylist[i])
        ws.write(i + 1, 2, inaccuracies[i])


def get_sub_depend(string,
                   substring,
                   dstr_len,
                   repeat,
                   func,
                   name,
                   subfunc=None):
    xlist = [i for i in range(0, len(substring) + 1, dstr_len)]
    ylist = get_measures(string, substring, func, repeat, xlist, subfunc)
    ws = wb.add_sheet(name)
    ws.write(0, 0, "Substring Length")
    ws.write(0, 1, "Search Time")
    for i in range(len(xlist)):
        ws.write(i + 1, 0, xlist[i])
        ws.write(i + 1, 1, ylist[i])


string_len = 10**5
substring_len = 20
dlen = 2000

# string = g.random_string(string_len)
# substring = g.random_string(20)


wb = xlwt.Workbook()

string, substring = g.random_string(string_len), g.random_string(substring_len)

# with open('.\measure_main\\random_string.txt', 'r', encoding='utf-8') as f:
#     x = f.read().split('\n\n')
#     string, substring = x[1], x[0]


repeat = 30
get_main_dependence(string, substring, dlen, repeat, s.Automaton, 'Automaton')
# get_main_dependence(string, substring, dlen, repeat, s.BoyerMoore, 'BoyerMoore')
# get_main_dependence(string, substring, dlen, repeat, s.BrutForce.find, 'BruteForce')
# get_main_dependence(string, substring, dlen, repeat, s.Raita, 'Raita')
# get_main_dependence(string, substring, dlen, repeat, s.KnuthMorrisPratt, "KMP")
# get_main_dependence(string, substring, dlen, repeat, s.Hash.find, 'Line Hash', s.line_hash)
# get_main_dependence(string, substring, dlen, repeat, s.Hash.find, 'Square Hash', s.square_hash)
# get_main_dependence(string, substring, dlen, repeat, s.Hash.find, 'RK', s.Rabin_Karp)
# get_main_dependence(string, substring, dlen, repeat, s.Hash.find, 'Power', s.power_hash)
wb.save('.\Measures.xls')
